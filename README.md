Thông tin chi tiết Máy xay thịt Tefal MB450B38:

https://hiyams.com/may-xay/may-xay-thit/may-xay-thit-tefal-mb450b38/

Máy xay thịt Tefal MB450B38 là thiết bị đa năng 3 trong 1, dùng để xay, cắt nhỏ và trộn các thành phần. Hoàn hảo để cắt nhỏ hành, tỏi và các loại thảo mộc. Đồng thời cũng có thể dùng để chuẩn bị nguyên liệu làm sốt mayonnaise và nước sốt.

Máy xay thịt Tefal MB450B38 3 trong 1 – xay, cắt, trộn, lý tưởng để băm hành tỏi và nguyên liệu để làm sốt mayonnaise và nước sốt. Được làm bằng 4 lưỡi dao bằng thép không gỉ chất lượng cao giúp xay và cắt nhỏ hiệu quả.

Máy xay thịt Tefal MB450B38 với động cơ 500W 1 tốc độ và mạnh mẽ cho kết quả xay hoàn hảo.

Thông số kỹ thuật Máy xay thịt Tefal MB450B38:

Công suất: 500W

Chức năng: 3 trong 1 (xay thịt, xay tiêu/tỏi/ớt, trộn thực phẩm)

Lưỡi dao máy xay: 4 cánh bằng thép không gỉ, giúp an toàn khi tiếp xúc thực phẩm

Tốc độ: 1 nhấn

Cối lớn: Bằng nhựa

Dung tích tổng: 0.5 L

Xuất xứ: Thổ Nhĩ Kỳ

Kích thước: 12.6 x 12.6 x 27.5 cm (DxRxC)

Khối lượng: 1.04kg

Bảo hành: 2 năm